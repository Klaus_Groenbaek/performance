package computerdatabase.perfromance

class Stateless extends Simulation {

  val httpConf = http
    .baseURL("http://localhost:8080") // Here is the root for all relative URLs

  val scn = scenario("Stateless")
    .repeat(10, "counterName") {
      exec(http("Page 1").get("/site/page1.html"))
        .exec(http("Page 2").get("/site/page2.html"))
        .exec(http("Page 3").get("/site/page3.html"))
        .exec(http("Page 4").get("/site/page4.html"))
    }
    setUp(scn.inject(rampUsers(10) over(10 seconds)).protocols(httpConf))
}
