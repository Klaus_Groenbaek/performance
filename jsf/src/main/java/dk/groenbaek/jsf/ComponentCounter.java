package dk.groenbaek.jsf;

import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

import javax.faces.component.visit.VisitContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 * PhaseListener that will visit every component and count the number of components
 * @author klaus
 *         Created 11/04/2017.
 */
@Log4j
public class ComponentCounter implements PhaseListener {

    private static final long serialVersionUID = 8573925562596090327L;

    @Override
    public void beforePhase(PhaseEvent event) {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        VisitContext visitContext = VisitContext.createVisitContext(facesContext);

        CountingVisitCallback callback = new CountingVisitCallback();
        facesContext.getViewRoot().visitTree(visitContext, callback);

        log.info(("Number of Components: " + callback.getCount()));
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }

    @Override
    public void afterPhase(PhaseEvent event) {
        // do nothing
    }
}
