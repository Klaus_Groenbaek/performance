package dk.groenbaek.jsf;

/**
 * @author klaus
 *         Created 11/04/2017.
 */

import javax.faces.component.UIComponent;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import java.util.ArrayList;
import java.util.List;

/**
 * VisitCallback that is used to gather information about the component tree.
 * Keeps track of the total number of components and maintains a list
 * of basic component information.
 */
public class CountingVisitCallback implements VisitCallback {

    private int count = 0;
    /**
     * This method will be invoked on every node of the component tree.
     */
    @Override
    public VisitResult visit(VisitContext context, UIComponent target) {

        count++;
        // descend into current subtree, if applicable
        return VisitResult.ACCEPT;
    }

    public int getCount() {
        return count;
    }


}
