package dk.groenbaek.jsf;

import dk.groenbaek.shoppingbasket.dto.Basket;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * @author klaus
 *         Created 11/04/2017.
 */
@SessionScoped
@Named(value = "basket")
public class JSFBasket extends Basket {
    public JSFBasket() {
        super(0);
    }
}
