package dk.groenbaek.jsf;

import dk.groenbaek.shoppingbasket.dao.ProductRepository;

import javax.ejb.Singleton;
import javax.inject.Named;

/**
 * @author klaus
 *         Created 11/04/2017.
 */
@Singleton
@Named("repository")
public class JSFProductRepository extends ProductRepository{
}
