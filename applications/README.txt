This application has a number of built in errors
There is a configuration error and a code error, and they are interrelated.
In order to find both you need to first fix the code error, so you can find the configuration error

To find the code error run the following scenario
Create a single user, and have him login 3000 times, watch as the response time grow.
The code error is not that obvious, so you may need to enable debug logging, or use JVisualJVM to figure out where
the time/memory is used

The configuration error is related to the number of concurrent users, the symptom is growing response time, but instead
of growing as a function of time, it is now growing as a function of concurrent user.


