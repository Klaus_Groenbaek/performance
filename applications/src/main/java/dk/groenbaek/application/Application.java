package dk.groenbaek.application;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Klaus Groenbaek
 */
@Slf4j
@SpringBootApplication
@EnableSwagger2
public class Application {

    public static void main(String[] args) throws Exception {

        SpringApplication.run(Application.class, args);
        log.info("Application started.");
    }

    /**
     * A bean that configures the Swagger2 api definitions. Here we specify that the Spring boot white-label error controller, should not be considered
     * an API endpoint. Remember the @EnableSwagger2 annotation on you configuration
     */
    @Bean
    public Docket docker() {
        return new Docket(DocumentationType.SWAGGER_2)
                .globalResponseMessage(RequestMethod.GET, ImmutableList.of(new ResponseMessage(200, "OK", null, new HashMap<>(), new ArrayList<>())))
                .globalResponseMessage(RequestMethod.POST, ImmutableList.of(new ResponseMessage(200, "OK", null, new HashMap<>(), new ArrayList<>())))
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(Predicates.not(PathSelectors.regex("/error"))) // Exclude Spring error controllers
                .build();
    }

}
