package dk.groenbaek.application.dto;

import lombok.Data;

/**
 * @author Klaus Groenbaek
 *
 */
@Data
public class AuthenticateRequest {
    private String userName;
    private String password;
}
