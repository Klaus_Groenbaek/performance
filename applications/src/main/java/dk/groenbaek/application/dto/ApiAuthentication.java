package dk.groenbaek.application.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Klaus Groenbaek
 *
 */
@Data
@Accessors(chain = true)
public class ApiAuthentication {
    private String token;
    private String error;
}
