package dk.groenbaek.application.jpa.repositories;

import dk.groenbaek.application.jpa.entities.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Klaus Groenbaek
 *
 */
@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    @Query("select t from Token t where t.token = :token")
    Token findToken(@Param("token") String token);
}
