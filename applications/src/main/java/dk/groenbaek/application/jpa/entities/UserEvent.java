package dk.groenbaek.application.jpa.entities;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Klaus Groenbaek
 *
 */
@Entity
@Getter
@Setter
@Accessors(chain = true)
public class UserEvent {

    public enum EventType{Login, Signup}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;

    @ManyToOne
    private RegisteredUser user;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventTime;

    @Enumerated(EnumType.STRING)
    @Column
    private EventType eventType;

    public UserEvent() {
        eventTime = new Date();
    }
}
