package dk.groenbaek.shoppingbasket.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * class is not thread safe and should not be used with concurrent requests on the same session
 * @author Klaus Groenbaek
 *
 */
@Data
public class Basket implements Serializable {

    private Map<Integer, LineItem> items = new HashMap<>();

    /**
     * Each basket gets an extra 128K so they take up more memory
     */
    private byte[] dummy;

    public Basket() {
        this(128000);
    }

    public Basket(int byteCount) {
        this.dummy =  new byte[byteCount];
    }

    public Basket addItem(Product product, int amount) {
        LineItem lineItem = items.get(product.getId());
        if (lineItem == null) {
            if (amount < 0) {
                throw new IllegalStateException("Quantiry can't be negative");
            }
            lineItem = new LineItem().setProduct(product);
            items.put(product.getId(), lineItem);
        }
        lineItem.incrementBy(amount);
        if (lineItem.getAmount() == 0) {
            items.remove(product.getId());
        }
        return this;
    }

    public double getTotal() {
        double sum = 0;
        for (LineItem lineItem : items.values()) {
            sum += lineItem.getAmount() * lineItem.getProduct().getPricePerItem();
        }
        return sum;
    }
}
