package dk.groenbaek.shoppingbasket.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Klaus Groenbaek
 *
 */
@Data
@Accessors(chain = true)
public class LineItem {
    private int amount;
    private Product product;

    public void incrementBy(int amount) {
        this.amount += amount;
    }
}

