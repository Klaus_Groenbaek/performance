package dk.groenbaek.shoppingbasket.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Klaus Groenbaek
 *
 */
@Data
@Accessors(chain = true)
public class Product {
    private int id;
    private String description;
    private double pricePerItem;
}
