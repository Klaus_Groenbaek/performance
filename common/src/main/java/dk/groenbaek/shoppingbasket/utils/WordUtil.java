package dk.groenbaek.shoppingbasket.utils;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Klaus Groenbaek
 *
 */
public class WordUtil {

    private static List<String> randomWords = Lists.newArrayList(
                "Cui", "Tubuli", "nomen", "odio", "non", "est", "minor", "inquit", "voluptas", "percipitur", "vilissimis",
                "rebus", "quam", "pretiosissimis", "sit", "hoc", "ultimum", "bonorum", "quod", "nunc",
                "defenditur", "usque", "quaque", "aliter", "vita", "cuius", "quidem", "quoniam", "stoicus",
                "fuit", "sententia", "condemnata", "mihi", "videtur", "esse", "inanitas", "ista", "verborum");


    public static List<String> randomStrings(int numberOfStrings, int wordCount) {

        Random rnd = new Random();
        int size = randomWords.size();
        List<String> result = new ArrayList<>();
        while (numberOfStrings > 0) {
            StringBuilder sb = new StringBuilder();
            for (int i=0; i < wordCount; i++) {
                int index = rnd.nextInt(size);
                sb.append(randomWords.get(index));
                if (i +1 != wordCount) {
                    sb.append(" ");
                }
            }
            result.add(sb.toString());
            numberOfStrings--;
        }
        return result;
    }

}
