package dk.groenbaek.shoppingbasket.dao;

import dk.groenbaek.shoppingbasket.dto.Product;
import dk.groenbaek.shoppingbasket.utils.WordUtil;
import lombok.Getter;

import java.util.*;

/**
 * @author klaus
 *         Created 11/04/2017.
 */

public class ProductRepository {

    /**
     * To be thread-safe the map reference must be final (and the map may not be changed later)
     */
    @Getter
    private final Map<Integer, Product> items = new HashMap<>();

    public ProductRepository() {
        List<String> descriptions = WordUtil.randomStrings(200, 4);
        int count = 0;
        Random rnd = new Random();
        for (String description : descriptions) {
            double price = rnd.nextInt(5000) / 100.0 + 10;
            int id = count++;
            items.put(id, new Product().setId(id).setDescription(id + " " + description).setPricePerItem(price));
        }
    }

    public Collection<Product> getProducts() {
        return items.values();
    }

    public Product getItem(int id) {
        return items.get(id);
    }

}
