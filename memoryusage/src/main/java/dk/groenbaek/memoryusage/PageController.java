package dk.groenbaek.memoryusage;

import dk.groenbaek.shoppingbasket.dao.ProductRepository;
import dk.groenbaek.shoppingbasket.dto.Basket;
import dk.groenbaek.shoppingbasket.dto.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * @author Klaus Groenbaek
 *
 */
@RequestMapping(value = "/")
@Controller
@SessionAttributes("basket")
public class PageController {

    @Autowired
    private ProductRepository products;

    @ModelAttribute("basket")
    private Basket getBasket(@ModelAttribute("basket")Basket basket) {
        if (basket == null) {
            basket = new Basket();
        }
        return basket;
    }

    @ModelAttribute("products")
    private Collection<Product> products() {
        return products.getProducts();
    }

    @GetMapping
    public String index() {
        return "index";
    }

    @GetMapping("/add/{id}/{amount}")
    public String addItem(@PathVariable("id") int productId, @PathVariable("amount") int amount, @SessionAttribute("basket") Basket basket) {
        Product product = products.getItem(productId);
        if (product == null) {
            throw new IllegalStateException("No product with id '" + productId + "'");
        }
        basket.addItem(product, amount);
        return "redirect:/";
    }
}
