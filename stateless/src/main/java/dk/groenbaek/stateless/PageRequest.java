package dk.groenbaek.stateless;

import lombok.Data;

import javax.validation.constraints.Min;

/**
 * @author Klaus Groenbaek
 *
 */
@Data
public class PageRequest {
    @Min(value = 0)
    private int pageNumber;
    @Min(value = 1)
    private int pageSize;

    public int start() {
        return pageNumber * pageSize;
    }

    public int end() {
        return start() + pageSize;
    }

}
