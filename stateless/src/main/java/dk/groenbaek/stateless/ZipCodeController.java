package dk.groenbaek.stateless;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Klaus Groenbaek
 *
 */
@Controller
@RequestMapping(value = "/zip", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
public class ZipCodeController {

    @Autowired
    private ZipCodeService zipCodeService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<ZipCode> getZipCodes() {
        return zipCodeService.getAll();
    }

    @RequestMapping(value = "/{section}", method = RequestMethod.GET)
    @ResponseBody
    public List<ZipCode> getZipCodes(@PathVariable("section") char c) {
        return zipCodeService.getSection(c);
    }


    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ResponseBody
    public PageResult getZipCodes(@Valid @RequestBody PageRequest request) {
        return zipCodeService.page(request);
    }





}
