angular.module('all', ['ui.bootstrap']);
var PaginationDemoCtrl = function ($scope, $http) {

    $scope.data = [];
    $scope.viewby = 10;
    $scope.totalItems = $scope.data.length;
    $scope.currentPage = 1;
    $scope.itemsPerPage = $scope.viewby;
    $scope.maxSize = 5; //Number of pager buttons to show

    $http({
        'url': 'zip',
        'method': 'GET',
        'accept': {
            'Content-Type': 'application/json'
        }
    }).then(function (response) {
        $scope.data = response.data;
        $scope.totalItems = $scope.data.length;
    }).catch(function (response) {
        alert("Failed to load data");
        console.error(response);
    });

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function () {
        console.log('Page changed to: ' + $scope.currentPage);
    };

    $scope.setItemsPerPage = function (num) {
        $scope.itemsPerPage = num;
        $scope.currentPage = 1;
    }
};