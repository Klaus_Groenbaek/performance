angular.module('paginated', ['ui.bootstrap']);
var PaginationDemoCtrl = function ($scope, $http) {

    $scope.data = [];
    $scope.viewby = 10;
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage = $scope.viewby;
    $scope.maxSize = 5; //Number of pager buttons to show
    $scope.numPages = 1;

    function load(page, size) {
        $http({
            'url': 'zip/page',
            'method': 'POST',
            'accept': {
                'Content-Type': 'application/json'
            },
            data: {
                'pageNumber' : page,
                'pageSize' : size
            }
        }).then(function (response) {
            console.log("data", response);
            $scope.data = response.data.results;
            $scope.totalItems = response.data.totalResults;
            $scope.viewby = response.data.pageSize;
            $scope.currentPage = response.data.pageNumber;
            $scope.itemsPerPage = $scope.viewby;


        }).catch(function (response) {
            alert("Failed to load data");
            console.error(response);
        });
    }

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function () {
        load($scope.currentPage, $scope.viewby);
        console.log('Page changed to: ' + $scope.currentPage);
    };

    $scope.setItemsPerPage = function (num) {
        $scope.itemsPerPage = num;
        $scope.currentPage = 1;
        $scope.pageChanged();
    };
    $scope.pageChanged();
};