package dk.groenbaek.cpuusage;

import lombok.SneakyThrows;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.*;

/**
 * @author Klaus Groenbaek
 *
 */
@Component
public class Requester {

    @Autowired
    private CloseableHttpClient closeableHttpClient;

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private HttpHost host;

    private Thread looper;

    private volatile boolean shutdown = false;

    @SneakyThrows(InterruptedException.class)
    public void shutdown() {
        shutdown = true;
        if (looper != null) {
            looper.join();
        }
    }

    /**
     * start a new thread that will initiate the first round of requests to a completion handler, when the future is
     * returned a new request is added to the queue to ensure that work is always ready when a task completes
     * @param numberOfThreads the number of threads to keep busy
     */
    public void makeRequests(int numberOfThreads) {
        looper = new Thread(new Runnable() {
            int count = 0;
            @Override
            @SneakyThrows(InterruptedException.class)
            public void run() {
                CompletionService<Void> completionService = new ExecutorCompletionService<>(executorService);

                for (int i = 0; i < numberOfThreads; i++) {
                    completionService.submit(new WorkTask());
                }
                while (!shutdown) {
                    Future<Void> future = completionService.take();
                    try {
                        completionService.submit(new WorkTask());
                        future.get();
                        if (count++ % 1000 == 0) {
                            System.out.print("completed " + (count - 1) + " requests.\r");
                        }
                    } catch (ExecutionException e) {
                        if (!shutdown) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        looper.start();
    }


    /**
     * Task that will make a single request and check that the response code is 200
     */
    private class WorkTask implements Callable<Void> {

        @Override
        public Void call() throws Exception {
            HttpGet get = new HttpGet("/data");
            try (CloseableHttpResponse execute = closeableHttpClient.execute(host, get)) {
                int statusCode = execute.getStatusLine().getStatusCode();
                if (200 != statusCode) {
                    System.err.println("Status code " + statusCode);
                }
                return null;
            }
        }
    }

}
