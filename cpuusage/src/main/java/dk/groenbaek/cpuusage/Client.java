package dk.groenbaek.cpuusage;

import org.apache.http.HttpHost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A client that makes a number of request to the server. when all requests have completed the Client is closed,
 * and the program terminates
 * @author Klaus Groenbaek
 *
 */
@Configuration
@ConditionalOnClass()
@Import(Requester.class)
public class Client {

    private static int concurrentThreads() {
        return 4;
    }

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(Client.class).web(false).run(args);
        Requester requester = context.getBean(Requester.class);
        requester.makeRequests(concurrentThreads());
        System.out.println("Program will continue until you press a 'q' (if running from gradle use CTRL+c).");
        int read = 0;
        do {
            read = System.in.read();
        } while (read != 'q');

        requester.shutdown();
        context.close();
    }


    @Bean(destroyMethod = "shutdownNow")
    ExecutorService executorService() {
        return Executors.newFixedThreadPool(concurrentThreads());
    }


    /**
     * Creates a closable HttpClient that does not validate https certificates
     */
    @Bean(destroyMethod = "close")
    CloseableHttpClient createClient() {
        RegistryBuilder<ConnectionSocketFactory> registryBuilder = RegistryBuilder.<ConnectionSocketFactory>create();
        try {
            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, (chain, authType) -> true).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, (s, sslSession) -> true);
            registryBuilder.register("https", sslsf);

        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            throw new RuntimeException("Unable to initialize SSL context for localhost.", e);
        }

        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registryBuilder.build());
        cm.setMaxTotal(concurrentThreads());
        cm.setDefaultMaxPerRoute(concurrentThreads());
        return HttpClients.custom().setConnectionManager(cm).build();
    }

    @Bean
    HttpHost httpHost() {
        return new HttpHost("localhost", 8443, "https");
    }

}
